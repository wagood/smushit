<?php
/**
* Authored by WAGOOD, wagood@yandex.ru
**/

if (!defined('_PS_VERSION_'))
	exit;

define('SMUSHIT_USER_AGENT', 'PRESTASHOP reSMUSHIT');
define('SMUSHIT_WINDOW', 5);
define('SMUSHIT_URL', 'http://api.resmush.it/ws.php');
/* set SMUSHIT_DEBUG and see request and responce from resmush.it server in /modules/smushit/log.txt file */
define('SMUSHIT_DEBUG', true);

class Smushit extends Module
{
	protected $config_form = false;
	private $imageTypes = array();
	protected $smushit_url;
	
	public function __construct()
	{
		$this->name = 'smushit';
		$this->tab = 'administration';
		$this->version = '1.0.3';
		$this->author = 'WAGOOD, wagood@yandex.ru';

		/**
		 * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
		 */
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('reSmushIt');
		$this->description = $this->l('Optimize images by reSmush.It service.');

		$this->confirmUninstall = $this->l('Are you sure?');

		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		
		foreach (ImageType::getImagesTypes('products') as $type)			
				$this->imageTypes[] = $type;
		
		$this->savelog = Configuration::get('SMUSHIT_SAVELOG');
		
		$this->smushit_url = SMUSHIT_URL;
		$configuration_jpeg_quality = (int)Configuration::get('PS_JPEG_QUALITY');
		if (isset($configuration_jpeg_quality) && $configuration_jpeg_quality > 0 && $configuration_jpeg_quality < 100)
			$this->smushit_url .= '?qlty='.$configuration_jpeg_quality;					
	}

	/**
	 * Don't forget to create update methods if needed:
	 * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
	 */
	public function install()
	{
		Configuration::updateValue('SMUSHIT_LIGHT_MODE', 0);
		Configuration::updateValue('SMUSHIT_SAVELOG', 1);
		Configuration::updateValue('SMUSHIT_LOG_SIZE', 0);
		
		$result = parent::install() &&	$this->registerHook('actionWatermark');
		
		$this->updatePosition(Hook::getIdByName('actionWatermark'), 0, 1);		
		
		return $result;
	}

	public function uninstall()
	{
		Configuration::deleteByName('SMUSHIT_LIGHT_MODE');
		Configuration::deleteByName('SMUSHIT_SAVELOG');
		Configuration::deleteByName('SMUSHIT_LOG_SIZE');
		
		return parent::uninstall() && $this->unregisterHook('actionWatermark');
	}

	/**
	 * Load the configuration form
	 */
	public function getContent()
	{
		/**
		 * If values have been submitted in the form, process.
		 */
		$this->postProcess();

		$this->context->smarty->assign(
				array('module_dir' => $this->_path,
						'text_saving' => $this->l('Total saved:').' '.Configuration::get('SMUSHIT_LOG_SIZE').' '.$this->l('bytes')
				)
		);

		$output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

		return $output.$this->renderForm();
	}

	/**
	 * Create the form that will be displayed in the configuration of your module.
	 */
	protected function renderForm()
	{
		$helper = new HelperForm();

		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$helper->module = $this;
		$helper->default_form_language = $this->context->language->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitMymoduleModule';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
			.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');

		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
		);

		return $helper->generateForm(array($this->getConfigForm()));
	}

	/**
	 * Create the structure of your form.
	 */
	protected function getConfigForm()
	{
		$title = $this->l('Settings');
		return array(
			'form' => array(
				'legend' => array(
				'title' => $title,
				'icon' => 'icon-cogs',
				),
				'input' => array(
					array(
						'type' => 'switch',
						'label' => $this->l('Light mode'),
						'name' => 'SMUSHIT_LIGHT_MODE',
						'is_bool' => true,
						'desc' => $this->l('If "Yes" only send one big image to optimize, if "No" send all images.'),
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							)
						),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				),
			),
		);
	}

	/**
	 * Set values for the inputs.
	 */
	protected function getConfigFormValues()
	{
		return array(
			'SMUSHIT_LIGHT_MODE' => Configuration::get('SMUSHIT_LIGHT_MODE', 0),
		);
	}

	/**
	 * Save form data.
	 */
	protected function postProcess()
	{
		$form_values = $this->getConfigFormValues();

		foreach (array_keys($form_values) as $key)
			Configuration::updateValue($key, Tools::getValue($key));
	}

	public function hookActionWatermark($params)
	{
		$images_list = array();
		$image = new Image($params['id_image']);
		$image->id_product = $params['id_product'];
		$file = _PS_PROD_IMG_DIR_.$image->getExistingImgPath().'.jpg';

		array_push($images_list, $file);
		
		if (Configuration::get('SMUSHIT_LIGHT_MODE', 0) == 0) 
		{
			//go through file formats and resize them
			foreach ($this->imageTypes as $imageType)
			{
				$newFile = _PS_PROD_IMG_DIR_.$image->getExistingImgPath().'-'.Tools::stripslashes($imageType['name']).'.jpg';
				array_push($images_list, $newFile);			
			}
		}
		
		return $this->smushitByImage($images_list);
	}

	private function smushitByImage($file)
	{		
		require_once _PS_MODULE_DIR_.$this->name.'/libs/RollingCurl/RollingCurl.php';
		$this->_images = $images_list = array();
		
		if (!is_array($file))
			$images_list[] = $file;
		else 
			$images_list = $file;
		
		// multi connect to urls
		$rc = new RollingCurl(array(&$this, 'requestCallback'));
		$rc->window_size = SMUSHIT_WINDOW;
		foreach ($images_list as $key => $image_file) 
		{
			$this->_images[$key] = array('src' => $image_file);
			$request = new RollingCurlRequest($this->smushit_url, $method = 'POST', array('files' => (self::getCurlValue($image_file))));
			$request->userdata = $key;
			$rc->add($request);
		}
		$rc->execute();
		$rc->__destruct();

	}	
	
	public function requestCallback($response, $info, $request)
	{
		if ($info['http_code'] <> '200')
			return false;
		
		$response = Tools::jsonDecode($response);
		
		if (SMUSHIT_DEBUG === true) {
			$output = print_r($response, true);
			$output .= print_r($request, true);
			$this->savetolog($output);
		}
	
		if (isset($response->error)) 
			return false;
	
		if (isset($response->dest) && isset($response->percent) && $response->percent > 0) 		
			if (Tools::copy($response->dest, $this->_images[$request->userdata]['src']) && $this->savelog)
				Configuration::updateValue('SMUSHIT_LOG_SIZE', Configuration::get('SMUSHIT_LOG_SIZE') + $response->src_size - $response->dest_size);

		return true;
	}
	
	private function savetolog($data) 
	{
		file_put_contents ( dirname(__FILE__).'/log.txt', $data, FILE_APPEND );
	}

	private static function getCurlValue($filename)
	{
		if (function_exists('curl_file_create'))
			return curl_file_create($filename);

		return '@'.$filename;
	}
}
